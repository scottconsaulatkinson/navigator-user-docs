==========
Workspaces
==========

What is a Workspace?
====================

A :term:`workspace <workspace>` provides a secure work area within the Navigator platform where your organization’s data can be added and shared with a set of specified users. 

As an Early Experience Program user of the Navigator platform, you have access to a private workspace on the platform visible only to you, your organization and Impact Observatory. Your private workspace is where you can manage users and upload custom places of interest for analysis.

The Navigator Workspace
=======================

As an EEP user you are automatically a viewer of the main Navigator workspace which contains a standard set of places, data layers, and indicators. The Navigator workspace includes all countries and regions of the world, over 200 global environmental datasets, and nine automatically calculated indicators to provide summary statistics over any country, region, or shape you upload to your private workspace. 


.. image:: ../images/select-workspaces.png
    :width: 600 px
    :alt: How to select workspaces.


Select a Workspace
==================

Alongside the Impact Observatory name at the top left panel, you can see all of the workspaces you are a part of by clicking the "Map View" drop down button. By default, if no workspaces are selected, the platform behaves as if all of the workspaces were selected. Selecting one workspace will limit the places and layers in the search results to the assets only in that workspace.

.. note:: We do not recommend unchecking the Navigator workspace as this will temporarily hide the indicator visualizations from the left hand panel when viewing places.

Access the Workspace Admin
==========================

.. image:: ../images/access-admin.png
    :width: 600 px
    :alt: How to access the admin.
	
Users with editor, admin or owner permissions can access a private :term:`workspace admin <workspace admin>`:
- Click via the MAP VIEW drop down menu. 
- Click the ADMIN button next to the workspace where the assets you would like to manage are stored. 


.. image:: ../images/nav-home.png
    :width: 600 px
    :alt: Home page.

When you enter the workspace admin, you will see a home page, with a summary dashboard of the workspace’s assets. Click on the drop down menu in the left hand panel to select the type of assets you would like to manage. Places, widgets, layers, users, dashboards, and workspaces can all be managed in the admin. 

-----------------------------------------
Manage Users and Places in Your Workspace
-----------------------------------------

Your workspace contains all of your custom :term:`places <place>`, or .geojson files. 

Create a New Place 
------------------

- In the places page of your workspace admin, click CREATE NEW PLACE.  
- Name the place
- Select the :term:`place type <place type>`
- Click GENERATE A SLUG NAME to automatically create a :term:`slug <slug>` name, this is a unique ID for the place
- Under the SHAPE FILE tab, click Choose File. Select a GeoJSON from your local machine. Navigator requires a GeoJSON file under 2MB, a coordinate reference system in WGS 84 EPSG 4326.
- SAVE AND VIEW DETAILS. 
- Turn on the Publish toggle for the place to be accessible from the Map View. Turn on the Featured toggle if you would like the place to appear at the top of search results. 

.. image:: ../images/create-place.png
   :width: 700 px
   :alt: Create New Place Menu

.. note:: Indicator load times: Once you save a place, indicator calculations are automatically triggered. The smaller the place, the faster the indicator will calculate, usually in a few seconds or minutes. If an indicator fails to load after a few hours, the place may be too large of an area. Try breaking up the place into smaller shapes within a GIS.  

.. note:: GeoJSON upload requirements: 1. file size under 2MB, 2. coordinate reference system in WGS 84 EPSG 4326 3.valid geometries. If you get an error saving the file, check that your shape meets the file requirements. If you continue to have problems, reach out to our team for technical support using the *Submit Feedback* button found under the account icon. 

Download GeoJSON for a Place
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: ../images/dwnld-geojson.png
   :width: 700 px
   :alt: GeoJSON Download Menu

Navigator allows users to download the GeoJSON file for a place to their local machine. This feature maintains congruence between analysis on the platform and any additional analysis or cartography GIS users desire to perform with other applications. 

To download the GeoJSON file for a place:

- Search for a place in the places admin
- Click the VIEW AND UPLOAD SHAPE button, found above the map of the shape
- Click Download GeoJSON


Hide a Place from the Map View: 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Search for the place in the places admin 
- Click the Published toggle to unpublish the place. This action can be undone by clicking the Published button again. When a place is unpublished it will remain in the places admin, but it will not appear in search results in the Map View. 

Permanently Delete a Place:	
~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Enter the Places admin
- Select the place you want to delete
- Scroll down to the bottom of the place’s metadata
- Click DELETE PLACE (this action cannot be undone)





Manage Users in Your Workspace
------------------------------
.. _User Permissions:

User Permissions
~~~~~~~~~~~~~~~~
As a member of a private workspace your account is assigned to one of four permission levels: owner, admin, editor, or creator. Roles and permissions are used to define what individual users can do within a workspace. User permissions can be edited at any time. 

- :term:`Viewers <viewer>` - can view all workspace assets, including places and layers, on the map view. Viewers can also view and download indicator results, clip and export data from a layer for any place, create collections, clip and export data from a collection. Viewers do not have access to the admin tool from where assets and users can be added, edited and removed. 

- :term:`Editors <editor>` - have all viewer permissions, and can manage assets via the admin tool. Editors do not have access to add or remove users. While not required, experience working with GIS or design software will better enable editors when:
    - uploading and removing places
    - editing layer styles and colors in the layer config box
    - editing layer legend styles and colors in the layer config box
    - editing layer and place metadata, such as title, filters, and description boxes 
    
- :term:`Admins <admin>` - have all viewer and editor permissions, can add and manage users, and assign roles to users as editors and viewers

Adding and Removing Users
~~~~~~~~~~~~~~~~~~~~~~~~~

Only admins and owners have the ability to add or remove :term:`users <user>`. 

.. image:: ../images/add-user.png
   :width: 800 px
   :alt: Add User Menu

To add a new user:

- Send the new user the following link to sign up for Navigator: https://login.demo.impactobservatory.com/ 
- Once the new user has verified their account via email, the existing user must add the new email address to the private workspace for the new user to gain accesss. This page is accessed via: MAP VIEW >> ADMIN >> HOME >> Users >> Add users to navigator
- Use the “Select role” drop down menu to choose the permission of the new user’s account. See `User Permissions`_ to determine which role to provide the user. 





