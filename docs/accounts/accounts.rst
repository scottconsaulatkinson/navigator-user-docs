======================
Accessing Your Account
======================

.. _creating-user-account:


As an Early Experience Program user of the :term:`Navigator <navigator>` platform, you have access to a private :term:`workspace <workspace>` on the platform visible only to you, your organization and Impact Observatory. Your private workspace will contain your organization’s :term:`places <place>` of interest and any :term:`layers <layer>` you provide the IO team to upload. 



Account Registration
--------------------

Follow the link provided by Impact Observatory to sign up for Navigator. After signing up, confirm your account via email and notify your team’s account admin. The admin will add your email address to your team’s private workspace. If you are the account admin for your team, notify Impact Observatory of the email address used to create your account and our team will create your private workspace. 


Log Into Your Account
---------------------

.. image:: ../images/log-in.png
        :width: 500 px
        :alt: Log-in page. 

Once you have an account, you can access the platform via `demo.impactobservatory.com <https://demo.impactobservatory.com>`_  to sign in. If you forget your password, a reset link can be sent to your email address on file. 

If you are unable to access your account, make sure your account email address has been added to your organization’s workspace by the account manager. If you are the account admin, reach out to Impact Observatory to establish yourself as the admin of the private workspace. 


Passwords
---------

Change Password
~~~~~~~~~~~~~~~
You can change your password from your Profile menu.


Forgot Password
~~~~~~~~~~~~~~~
If you forgot your password, you use the Forgot Password feature on Sign-In form.

A message to confirm you want to change your password is sent to your email address on file.



