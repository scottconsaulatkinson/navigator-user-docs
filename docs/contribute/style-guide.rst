
=====================================
Contribute to the Navigator User Docs
=====================================

The Navigator User Documentation is an open source project. We encouradge platform users to add to this documentation as they see fit. 

Style Guide
===========


The following styles should be used when improving the user documentation:

The docs directory contains a sub-directory for each major topic of the user documentation. Within each sub-directory is a single .rst file from where the documentation is written using Sphinx. 

Each page begins with a title using the equals sign (=) for Header #1 above and below the title. 

The second header uses a string of equals signs (=======) in the line below the phrase. Any phrase underlined in equals signs with no spaces or indents in front creates an entry in the table of contents. 

To create a sub-section within a secondary header, use a string of dashes (------) in the line below the phrase. 

To create a sub-section in a sub-section, use a string of tildes (~~~~~~~) in the line below the phrase. 

To create a bullet point list use a dash and a space (- ) followed by the phrase.

.. tip:: Use tips to add helpful insights. 

.. note:: Use notes for common gotchas. 



