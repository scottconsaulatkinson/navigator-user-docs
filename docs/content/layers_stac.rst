Configuring a STAC based layer for Navigator
============================================

Guide to the processes of configuring a STAC based layer on the Navigator platform.

Pre-requisites:

* The process starts from an original GeoTif file
* Access to Azure Blob Storages - file storage
* Available STAC instance - persistence layers for assets
* Available TiTiler instance - used for generating map tiles, rendered on the map view
* Python environment and GDAL installed - used for cog conversion

Converting the GeoTif to a Cloud Optimised GeoTiff (cog)
-------------------------------------------------------

The entire ecosystem relies on working with COGs. 

.. _gdal_translate: https://gdal.org/programs/gdal_translate.html

GDAL offers the utility function `gdal_translate`_ to convert a GeoTiff to COG:

.. code-block:: shell

    gdal_translate PATH_TO_GEO_TIFF OUTPUT_FILE -of COG -co COMPRESS=LZW -co BIGTIFF=IF_NEEDED

The resulted cog file then gets uploaded on Azure Blob Storage, where it can be referenced later. 

Once uploaded, keep track of the cog URL.

.. image:: images/nav_layer_azure_storage_explorer.png
   :width: 600 px
   :alt: Azure Storage Exloper

Preparing a STAC collection
---------------------------

Before uploading the cog reference as a STAC item, you need to prepare a STAC 
collection, a simple flexible JSON file that provides a structure to organize 
and browse one or multiple STAC Items.

A STAC collection has additional information such as the ``extents``, ``license``, ``keywords``, 
``providers``, etc that describe STAC Items that fall within the Collection.

* **id:** unbl-marine-pollution-index-rescaled
* **title:** unbl-marine-pollution-index-rescaled
* **license:**  CC BY 1.0
* **spatial extent:**  ``[-179.999998694099,-57.964209813775255,179.99944550757888,84.99582458608734]``
* **temporal extent:**  ``2013-01-01T00:00:00.000Z - 2014-01-01T00:00:00.000Z``

Note: the spatial and temporal extent aren't directly related with the asset (cog) 
you're trying to upload. These field should match the reunion of all the assets 
inside the collection.

:doc:`Here’s an example of details that we store on a STAC Collection <stac_catalog_example>` 

Creating a STAC based layer on the Navigator Platform
-------------------------------------------------------

On the navigator platform, in order to create a STAC based layer, you need to provide
the following information:

* **Layer Provider:** IO STAC Service (this will update the UI with STAC specific fields)
* **STAC API URL:** the root url for your STAC API instance, backed by a STAC PostgreSQL database 
* **Collection:** the id of the collection where you uploaded the STAC item
* **Asset:**  the name of the cog asset 
* **Date range:** of the cog asset (specified in the properties when creating the STAC item)
* **Tiler URL:** A Titiler instace URL from where the Navigator can generate tiles
* **Rescale:** the min and max data values the cog contains (e.q. 0 - 32)
* **Layer styling:**
  
  * Allows you to configure coloring breakpoints
  * These breakpoints **must have a value between 0 and 1**
  * These values are directly mapped with the rescale min and max value
  * 0 <-- 0
  * 1 <-- 32

.. image:: images/nav_layer_add_panel.png
   :width: 600 px
   :alt: Layer sources and meta-data


.. image:: images/nav_layer_rescale.png
   :width: 600 px
   :alt: Creating a Color Map


.. image:: images/nav_layer_stac_styling.png
   :width: 600 px
   :alt: Styled Layer from STAC meta-data
