============
The Map View
============

Upon launching the platform, you will enter the :term:`map view <map view>`. The map view is where you can search for places or layers to extract insights from environmental data about any area of the world. 

.. image:: ../images/map-view.png
    :width: 800 px
    :alt: The Map View. 



Places Tab on Navigator
=======================
	
As an Early Experience User, users have access to a catalogue of places within the Navigator workspace. Navigator contains over 4,900 places representing all recognized countries and regions to monitor and track environmental trends in any given corner of the world. 

Search for a place
------------------
From the PLACES tab, type in the name of a country or region. Only the places within your selected workspaces will appear in the search results (click on the Map View button to select or deselect additional workspaces to edit the search results). 
  
When you select a location from the search results, the map will automatically zoom to the area of interest and calculate the platform’s standard set of metrics for that place. 
  
You can also search within the PLACES tab using the filters box. Expand the Filters button, and select your filter of interest. You then can select the desired place from the search result list. 


.. tip:: 
  See `Download Indicator Results`_ to learn how to take advantage of indicators for further analysis. See `Create a New Place <https://navigator-user-docs.readthedocs.io/en/latest/workspaces/workspaces.html#create-a-new-place>`_ to learn how to run indicators over your areas of interest. 


Source for Places on Navigator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All shapes are from the `Who’s On First <https://whosonfirst.org/>`_ shape gazetteer which compiles a variety of open data sources to create a complete global set of country and region shapes.

.. tip::
   Visit `Who’s On First <https://whosonfirst.org/>`_ to find and download a free .geojson for nearly any place on Earth! Upload your geojson to the IO Platform to calculate custom metrics over your area of interest. See `*Create a Place* <https://whosonfirst.org/>`_ to learn how. 


Your places
-----------

Within your private workspace you can create a set of places that are of interest to you and your organization. The standard set of indicators included in the main Navigator workspace are also calculated for your places within your private workspace.  

Places in your private workspace will appear in the search results of the places tab. If you are not seeing your places in the search results, you may need to select your workspace from the drop down menu under the MAP VIEW button. Or, if you have no workspaces selected, the search behaves as if you have all the workspaces selected. 



Layers Tab on Navigator 
=======================

As an Early Experience User, users have access to a catalogue of over 100 global datasets, or layers, to monitor and track environmental trends anywhere in the world. This standard set of global layers is stored within the Navigator workspace. 

Search for a layer
------------------

From the LAYERS tab, type in the name or part of a name of a dataset. Only the layers within your selected workspaces will appear in the search results (click on the Map View button to select or deselect additional workspaces to edit the search results). 

When you select a layer from the search results, it will load on the map. You can also search within the PLACES tab via the FILTERS button to filter the search results by a dataset’s associated category. 

Layer Controls
~~~~~~~~~~~~~~

Once a layer is on the map, the map controls to zoom, turn on labels and roads, change the basemap, or re-center over your selected place.

.. image:: ../images/layer-controls.png
   :width: 400 px
   :alt: Layer controls can be found on the legend of a layer. 


A layer’s legend contains additional controls to edit the layer transparency, completely hide the layer, read a description of the layer and source information, and remove the layer from the map. 

Users may add multiple layers to the map simultaneously and alter their order using the leftmost icon in the legend. Try changing the transparency of one layer to see how information from two layers overlaps. 

About layers on Navigator
~~~~~~~~~~~~~~~~~~~~~~~~~

Additional information is available by clicking on the “i” button in a layer's legend including:
  - A description of the data
  - Source
  - Suggested Citation 
  - Download the Data hyperlink 
  - Read the Paper hyperlink 
  - License

  This information can be edited in the admin by editors, admins and owners.

Download the Data from an Original Source
-----------------------------------------

In an effort to reduce friction in locating sources of global data downloads, the information box almost every layer on Navigator contains a link to its open access download link. 

.. tip::
   Navigator can be used as a source of reference for the location of over 100 key environmental datasets on the internet.
   
To find the link for a certain layer:
- Add the layer to the Map View
- In the legend, the  “i” button opens a pop up window with information about a layer. The Download the Data hyperlink leads to an open access download of the layer. If a Download the Data link is not listed, the layer is not available for download from an outside source.  


Clip and Export Data 
====================

You can use the :term:`clip and export layer <clip and export>` button to download the data over a place:

.. image:: ../images/clip-export.png
   :width: 400 px
   :alt: Clip and Export Layers

- Select the PLACES tab in the left hand panel, search for and select your desired place.
- Click on the “More Options” button next to the title of the place to open the “Click and Export Layer” drop down.
- A new tab will open in the left hand panel. Search for and select your layer of interest. If your layer contains reference layers, such as multiple years or multiple versions of the data, you will need to select one of the reference layers from a second drop down menu. 
- Select the download format. Currently the platform only supports geotiff format. 
- Click DOWNLOAD to clip and export the layer to the current place. The raster data will save to the downloads folder on your local machine. Some layers are not available for download due to license restrictions. 




Map Controls
============

The MAP CONTROLS button in the bottom right corner of the map allows you to edit the :term:`basemap <basemap>`, zoom in or out, turn on or off labels and roads, and recenter the map over the selected place. Labels and roads are provided by Mapbox.

.. image:: ../images/map-controls.png
   :width: 200 px
   :alt: Map controls pop-up window, found in the bottom right corner. 


Basemaps
--------

The :term:`basemap <basemap>` can be set to a grayscale map style or a satellite basemap from Mapbox. No specific dates on specific imagery tiles can be provided by Mapbox at this time. Satellite basemap images are from a variety of satellites and range from a few years to a few months old.
