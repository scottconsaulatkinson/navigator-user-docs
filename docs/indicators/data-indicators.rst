===============
Data indicators
===============
.. image:: ../images/indicators-graphic.png
    :width: 800 px
    :alt: Example indicators image. 

What is an Indicator?
=====================

:term:`Indicators <indicator>` are a revolutionary approach to GIS. Indicators are automatically calculated summary statistics over any given area of interest. Indicators appear as interactive visualizations on the left hand panel of the map view when you select a place. 

You can download indicator results via the download button next to each visualization for further analysis outside of the platform. 
	
The Standard Indicators
-----------------------

The Navigator workspace includes nine standard Indicators to provide an instant analysis of the historical and present day environmental status of any given place. Navigator automatically calculates summary statistics for the following datasets: 

- **Annual Accumulated Tree Cover Loss (GFW Annual Tree Loss)**
  Indicator measures tree cover loss in square kilometers within a given place at a 30 meter resolution for each year from 2001 to 2019. 

.. image:: ../images/Tree_Loss_Metric.png
    :width: 400 px
    :alt: Tree Loss Metric. 


- **Biodiversity Intactness Index (UNEP-WCMC)**
  Indicator calculates the average amounts of high, medium and low biodiversity remaining within a given place in 2015 at a 300 meter resolution.

.. image:: ../images/BII_Metric.png
    :width: 400 px
    :alt: BII Metric.

- **Global Land Cover (ESA)**
  Indicator measures the percent of each land use taxonomy within a given place in 2019 according to ESA’s global Land Use Land Cover (LULC) map at a 300 meter resolution.

.. image:: ../images/ESA_LULC_Metric.png
    :width: 400 px
    :alt: ESA LULC Metric.

- **Enhanced Vegetation Index  (NASA MODIS)**
  Indicator calculated the sum of the total vegetation productivity within a given place for each year from 2000 to 2019 at a 500 meter resolution.

.. image:: ../images/EVI_Metric.png
    :width: 400 px
    :alt: EVI Metric.

- **Terrestrial Carbon Density (UNEP-WCMC, 2019)**
  Indicator calculates the total amount of carbon in millions of metric tonnes stored aboveground (biomass) and belowground (soil) within a given place for the year 2020 at a 300 meter resolution.

.. image:: ../images/WCMC_Carbon_Metric.png
    :width: 400 px
    :alt: WCMC Carbon Metric.

- **Terrestrial Human Footprint (WCS)** 
  Indicator measures the direct and indirect human pressures altering the natural state of the environment within a given place at a 1 kilometer resolution for the years 2000, 2005, 2010, 2013.

.. image:: ../images/HFP_Metric.png
    :width: 400 px
    :alt: Human Footprint Metric.

- **Monthly Fire Activity (NASA MODIS)**
  Indicator calculates and compares the total spatial extent of weekly fires in square kilometers within a given place throughout the year 2020 at a 500 meter resolution. 

.. image:: ../images/Fire_metric.png
    :width: 400 px
    :alt: Fire Metric.

- **Protected Areas (WDPA)** 
  Indicator calculates the percent and square kilometers within the given area that are classified as a Protected Area according to the IUCN. 

.. image:: ../images/WDPA_Metric.png
    :width: 400 px
    :alt: WDPA Metric.

Download Indicator Results
==========================

Indicator results are available for download via the Download Button next to each visualization for further analysis. Results can be downloaded as a CSV or JSON file for analysis in a wide range of applications. We suggest using Microsoft Excel or Google Sheets to generate tables and custom graphs for your specific use cases such as a report or presentation. 

.. image:: ../images/download-indicator-results.png
    :width: 600 px
    :alt: download-indicator-results.

.. tip:: 
  You can download indicator results for further analysis from multiple places at once using `Collections <https://navigator-user-docs.readthedocs.io/en/latest/collections/collections.html#collections>`_ 
	
More Information on Indicators
------------------------------

Next to the Download Button, the “i” button opens a window with additional information about the indicator including:

- Description of the calculation 
- Layers used to calculate the indicator
- Read the Paper link for the associated data
- Download the Data link for the associated data
- Source of the associated data 
- Suggested Citation
- License for the associated data

For more information on the creation of indicators and the calculations behind them, please contact our team using *Submit Feedback* button found under the accounts icon.


Share an Indicator
==================

Impact Observatory encourages users to use the Indicators for their reports and presentations. We suggest using the following methods: 

- Screenshot an indicator or the map view to create a png
- Copy and paste the url while on a specific place and layer combination to revisit the same view at a later time. Note that the latitude and longitude coordinates are included in the url as well. 
- Conduct a live demonstration of a place of interest

When incorporating data downloaded directly from Navigator or using screenshots of the Navigator platform, credit Impact Observatory as follows: 

*Impact Observatory Inc.* (2021). Navigator Platform. www.impactobservatory.com 

.. note:: 
  Live demos of Navigator greatly benefit from preloading places you plan to
  show to reduce load times and checking that indicators are functioning
  properly. If you would like to perform a live demo of our platform to your
  organization or clients, please notify gabriella@impactobservatory.com so that
  one of our team members will be made available to you for assistance if
  needed. 


