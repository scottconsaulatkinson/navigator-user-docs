
===========
Collections
===========

What is a Collection?
=====================

A :term:`collection <collection>` is a user defined group of places. Collections allow you to quickly download indicator results from a set of places. 
	   
Create a Collection
-------------------

.. image:: ../images/create-collection.png
   :width: 400 px
   :alt: Create Collection Menu

- Select the PLACES tab in the left hand panel on the map view. 
- Click CREATE NEW next to the word Collections to create a new collection.
- Name the collection, and select the workspace where the places you would like to include in the collection are stored. A single collection cannot contain places from more than one workspace. See `Managing places in your workspace <https://navigator-user-docs.readthedocs.io/en/latest/workspaces/workspaces.html#manage-places-in-your-workspace>`_ to learn how to add additional places for analysis to your private workspace. 
- Click CREATE NEW, then click ADD PLACES to search for the places you would like to add to the collection. Click SAVE. 
- Click EDIT to add or remove places from the collection. Use the More Options button to rename or delete the collection. To find the collection at a later time, search for the name in the places search bar. 

Download Data from a Collection
-------------------------------

.. image:: ../images/dwn-collection.png
   :width: 400 px
   :alt: Download Collection Menu


- To find a collection, search for its name in the places search bar.
- Click DOWNLOAD METRIC DATA FILES to download the results from the indicators onto your local machine 
- Select one or more indicators to download 
- Select CSV or JSON file

Sample Workflow
=============== 

.. tip:: Watch the `IO Navigator Platform Demo Video <https://vimeo.com/manage/videos/576948693>`_

Begin by selecting the places tab if not already highlighted. Search for Brazil. Select the country of Brazil from the search results. When you select Brazil, the map view will zoom to Brazil and bring up the dashboard of Navigator's standard Indicators. Scroll through the Indicators to explore the country’s environmental trends. 

